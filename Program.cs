﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace ConsoleApplication10
{
    class Program
    {
        static void Main(string[] args)
        {
            // site - polygon
            // buffer - polygon
            // datasets [
            // {
            //   name = "boreholes" 
            //   features collection = [geometry, attributes[]]
            // }]
            //   features collection - [geometry, attributes[]]
            // base mapping

            var wktReader = new WKTReader();
            var sitePoint = wktReader.Read("POINT (342755.0 147507.0)");
            var sitePolygon = wktReader.Read(
                    "POLYGON ((342780.0 147507.0, 342779.9391 147508.74391, 342779.7567 147510.47933, 342779.45369 147512.19779, 342779.03154 147513.89093, 342778.49232 147515.5505, 342777.83864 147517.16842, 342777.07369 147518.73679, 342776.2012 147520.24798, 342775.22542 147521.69463, 342774.15111 147523.06969, 342772.9835 147524.36646, 342771.72827 147525.57862, 342770.39154 147526.70027, 342768.97982 147527.72594, 342767.5 147528.65064, 342765.95928 147529.46985, 342764.36516 147530.1796, 342762.72542 147530.77641, 342761.04805 147531.25739, 342759.3412 147531.62019, 342757.61321 147531.86305, 342755.87249 147531.98477, 342754.12751 147531.98477, 342752.38679 147531.86305, 342750.6588 147531.62019, 342748.95195 147531.25739, 342747.27458 147530.77641, 342745.63484 147530.1796, 342744.04072 147529.46985, 342742.5 147528.65064, 342741.02018 147527.72594, 342739.60846 147526.70027, 342738.27173 147525.57862, 342737.0165 147524.36646, 342735.84889 147523.06969, 342734.77458 147521.69463, 342733.7988 147520.24798, 342732.92631 147518.73679, 342732.16136 147517.16842, 342731.50768 147515.5505, 342730.96846 147513.89093, 342730.54631 147512.19779, 342730.2433 147510.47933, 342730.0609 147508.74391, 342730.0 147507.0, 342730.0609 147505.25609, 342730.2433 147503.52067, 342730.54631 147501.80221, 342730.96846 147500.10907, 342731.50768 147498.4495, 342732.16136 147496.83158, 342732.92631 147495.26321, 342733.7988 147493.75202, 342734.77458 147492.30537, 342735.84889 147490.93031, 342737.0165 147489.63354, 342738.27173 147488.42138, 342739.60846 147487.29973, 342741.02018 147486.27406, 342742.5 147485.34936, 342744.04072 147484.53015, 342745.63484 147483.8204, 342747.27458 147483.22359, 342748.95195 147482.74261, 342750.6588 147482.37981, 342752.38679 147482.13695, 342754.12751 147482.01523, 342755.87249 147482.01523, 342757.61321 147482.13695, 342759.3412 147482.37981, 342761.04805 147482.74261, 342762.72542 147483.22359, 342764.36516 147483.8204, 342765.95928 147484.53015, 342767.5 147485.34936, 342768.97982 147486.27406, 342770.39154 147487.29973, 342771.72827 147488.42138, 342772.9835 147489.63354, 342774.15111 147490.93031, 342775.22542 147492.30537, 342776.2012 147493.75202, 342777.07369 147495.26321, 342777.83864 147496.83158, 342778.49232 147498.4495, 342779.03154 147500.10907, 342779.45369 147501.80221, 342779.7567 147503.52067, 342779.9391 147505.25609, 342780.0 147507.0))");
            var mbr = sitePoint.Buffer(300).Envelope;

            var writer = new GeoJsonWriter();
            var mbrJson = writer.Write(mbr);
            var mbrJObject = JObject.Parse(mbrJson);

            var features = new FeatureCollection();

            var boreholePoint = new Point(342760, 147520);
            var boreholeAttributes = new AttributesTable();
            boreholeAttributes.AddAttribute("symbology", "borehole");
            boreholeAttributes.AddAttribute("label", "1");
            features.Add(new Feature(boreholePoint, boreholeAttributes));

            var boreholePoint2 = new Point(342660, 147430);
            var boreholeAttributes2 = new AttributesTable();
            boreholeAttributes2.AddAttribute("symbology", "borehole");
            boreholeAttributes2.AddAttribute("label", "2");
            features.Add(new Feature(boreholePoint2, boreholeAttributes2));


            var bufferPolygon = sitePolygon.Buffer(250);
            var bufferAttributes = new AttributesTable();
            bufferAttributes.AddAttribute("symbology", "buffer");
            features.Add(new Feature(bufferPolygon, bufferAttributes));

            var siteAttributes = new AttributesTable();
            siteAttributes.AddAttribute("symbology", "site");
            features.Add(new Feature(sitePolygon, siteAttributes));            
            
            var featuresJson = writer.Write(features);
            var featuresJObject = JObject.Parse(featuresJson);

            var map = new
            {
                Width = 500,
                Height = 500,
                BaseMapping = "streetview",
                Mbr = mbrJObject,
                Features = featuresJObject
            };

            var serializer = new JsonSerializer()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var mapJObject = JObject.FromObject(map, serializer);
            var json = mapJObject.ToString(Formatting.None);
        }
    }

}
